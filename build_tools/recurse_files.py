import os
import argparse
import subprocess
import shlex

def traverse_directory(path, excluded, exec):
  if path in excluded:
    return
  for item in os.listdir(path):
    item_path = os.path.join(path, item)
    if os.path.isdir(item_path):
      traverse_directory(item_path, excluded, exec)
    else:
      if not item_path.endswith('.py'):
        continue
      split_args = shlex.split(exec)
      split_args = [item_path if arg == '{}' else arg for arg in split_args]
      subprocess.run(split_args)

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('paths', nargs='+')
  parser.add_argument("--exclude_in")
  parser.add_argument("--exec")
  args = parser.parse_args()

  with open(args.exclude_in, "r") as f:
    excluded = set()
    for line in f.readlines():
      line = line.rstrip()
      assert line.endswith('/*')
      excluded.add(os.path.abspath(line[:-2]))
  
  for path in args.paths:
    traverse_directory(os.path.abspath(path), excluded, args.exec)

if __name__ == "__main__":
  main()
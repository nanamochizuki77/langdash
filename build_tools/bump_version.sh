#!/bin/bash
version="$1"
if [[ "$version" == "" ]]; then
echo "version not specified"
exit 1
fi
sed -i "s/^__version__ = .*/__version__ = \"$version\"/" langdash/__init__.py
git add .
git commit -m "bump to $version"
git tag "$version"

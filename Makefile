.PHONY: build upload install format format-strings

build:
	-rm dist/*
	python -m build --sdist

upload: build
	twine upload --repository langdash dist/*

install: build
	pip install ./dist/*.tar.gz

PY_DIRS = ./langdash ./examples ./docs
format:
	yapf -i --recursive $(PY_DIRS)

format-strings:
	python build_tools/recurse_files.py --exec 'python build_tools/unify_quotes.py -i --quote "\"" "{}"' --exclude_in ./.yapfignore $(PY_DIRS)

type-check:
	mypy langdash --exclude 'langdash/extern'

lint:
	flake8 langdash

isort:
	isort langdash --sg 'langdash/extern/*'

import sys
import _import_helper
import _instruct_format
import readline
from langdash import Langdash
from langdash.infer import InferArgs, SpecialToken
from langdash.response import RespInfer
from langdash.chat import ChatCompletion, Role

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

init_session = ld.session_for_model("model", token_healing=False)
init_session.skip_bos = True

cc = ChatCompletion(
  init_session=init_session,
  chain_by_role={
    Role.User:
      ld.chain(
        args={"content": str},
        nodes=[
          SpecialToken("<s>"),
          ld.format_args("[INST] {content} [/INST]"),
        ]
      ),
    Role.Bot:
      ld.chain(
        args={"content": str},
        returns={"content": str},
        nodes=[
          ld.returns(
            "content",
            SpecialToken("</s>"),
            padleft=" ",
            infer_args=InferArgs(top_k=40, top_p=0.95),
            maybe_from_args=True,
          ),
          SpecialToken("</s>"),
        ]
      )
  }
)

chat_session = cc.session(max_context_length=4000)

while True:
  content = input("You: ")

  if content == "!regen":
    stream = chat_session.stream(
      role=Role.Bot, save_state=True, load_last_state=True
    )
  else:
    chat_session.call(role=Role.User, content=content)
    stream = chat_session.stream(role=Role.Bot, save_state=True)

  print("Bot: ", end="", flush=True)
  for resp in stream:
    if isinstance(resp, RespInfer):
      print(resp.tokstr, end="", flush=True)
  print()

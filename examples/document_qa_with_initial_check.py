import _import_helper
import _instruct_format

from langdash import Langdash
from langdash.search.embedding_search import EmbeddingSearch
from langdash.infer import InferArgs
from langdash.chains.typing import Mapping
from langdash.response import RespInfer

args = _import_helper.get_model_args()
prompt_format = _instruct_format.prompts[args["prompt_format"]]
prompt_end = _instruct_format.prompts_end.get(args["prompt_format"], "")

ld = Langdash()
ld.register_model(
  name="generation_model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)
ld.register_model(
  name="embedding_model",
  model=Langdash.model_from_type(
    "sentence_transformers", "multi-qa-MiniLM-L6-dot-v1"
  )
)

db = EmbeddingSearch(ld.session_for_model("embedding_model"))

db.add(
  [
    "The Eiffel Tower is a wrought-iron lattice tower on the Champ de Mars in Paris, France. It is named after the engineer Gustave Eiffel, whose company designed and built the tower.",
    "The design of the Eiffel Tower is attributed to Maurice Koechlin and Émile Nouguier, two senior engineers working for the Compagnie des Établissements Eiffel. It was envisioned after discussion about a suitable centerpiece for the proposed 1889 Exposition Universelle, a world's fair to celebrate the centennial of the French Revolution. Eiffel openly acknowledged that inspiration for a tower came from the Latting Observatory built in New York City in 1853.",
    "The proposed tower had been a subject of controversy, drawing criticism from those who did not believe it was feasible and those who objected on artistic grounds. Prior to the Eiffel Tower's construction, no structure had ever been constructed to a height of 300 m, or even 200 m for that matter, and many people believed it was impossible.",
    "The Eiffel Tower was the world's tallest structure when completed in 1889, a distinction it retained until 1929 when the Chrysler Building in New York City was topped out. The tower also lost its standing as the world's tallest tower to the Tokyo Tower in 1958 but retains its status as the tallest freestanding (non-guyed) structure in France.",
    "More than 300 million people have visited the Eiffel tower since it was completed in 1889. In 2015, there were 6.91 million visitors. The tower is the most-visited paid monument in the world. An average of 25,000 people ascend the tower every day which can result in long queues.",
    "As one of the most famous landmarks in the world, the Eiffel Tower has been the inspiration for the creation of many replicas and similar towers. An early example is Blackpool Tower in England. The mayor of Blackpool, Sir John Bickerstaffe, was so impressed on seeing the Eiffel Tower at the 1889 exposition that he commissioned a similar tower to be built in his town. It opened in 1894 and is 158.1 m (519 ft) tall. Tokyo Tower in Japan, built as a communications tower in 1958, was also inspired by the Eiffel Tower.",
  ]
)

init_chain = ld.chain(
  args={
    "document": str,
    "question": str
  },
  returns={
    "answer": str
  },
  nodes=[
    ld.format_args(
      prompt_format.format(
        instruction="""\
You are given a passage:

{document}

Based on the passage above, answer the following question. If it can't be answered based solely on the passage above, say "No".

Question:

{question}"""
      )
    ),
  ]
).cached("generation_model")

check_chain = ld.chain(
  returns={
    "answerable": Mapping({
      "Yes": True,
      "No": False
    }),
  },
  nodes=[
    "Can this question be answered based on the passage alone? ",
    ld.choice("answerable", choices=["Yes", "No"], argmax=True),
  ]
)

answer_chain = ld.chain(
  returns={"answer": str},
  nodes=[
    "Answer: ",
    ld.returns(
      "answer",
      prompt_end,
      infer_args=InferArgs(
        top_k=40,
        top_p=0.95,
        temperature=0.7,
      )
    ),
  ]
)


def do_inference(question):
  try:
    for (_, document, _) in db.search(question, max_documents=-1):
      print("Document for inference:")
      print(document)
      print("---------")

      result, session = init_chain.call(
        args={
          "document": document,
          "question": question
        }, return_session=True
      )

      with session.scratch_state():
        result = check_chain.call(session)
        print(result)
        if not result.returns["answerable"]:
          print("Not answerable... skipping document")
          continue

      for resp in answer_chain.stream(session):
        if isinstance(resp, RespInfer):
          print(resp.tokstr, end="", flush=True)

      print("\n")
      return
  except KeyboardInterrupt:
    print("(cut)")


def ml_input(prompt):
  result = ""
  line = input(prompt)
  if line.startswith("\\+"):
    line = line[1:]
  while True:
    if line.endswith("\\"):
      result += line[:-1] + "\n"
      line = input(">> ")
      continue
    else:
      result += line + "\n"
      break
  return result.rstrip()


while True:
  question = ml_input(">> Question: ")
  do_inference(question)

import os
import pickle
import readline
import _import_helper
import _instruct_format
from langdash import Langdash
from langdash.infer import InferArgs
from langdash.response import RespInfer

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

instruct_chain = ld.chain(
  args={"instruction": str},
  returns={"response": str},
  nodes=[
    ld.format_args(_instruct_format.prompts[args["prompt_format"]]),
    ld.returns("response", "", infer_args=InferArgs(
      top_k=40,
      top_p=0.95,
    )),
  ],
)

cached_instruct_chain = instruct_chain.cached("model", token_healing=False)

if "CACHE_STORE_FILE" in os.environ:
  CACHE_STORE_FILE = os.path.abspath(os.environ["CACHE_STORE_FILE"])
else:
  import tempfile
  CACHE_STORE_FILE = os.path.join(tempfile.gettempdir(), "instruct-cache.pkl")

print(
  f"""Special commands:
+load: load previous instruction cache
+save: save instruction cache to {CACHE_STORE_FILE}
+prompt [file]: load prompt from file
"""
)


def ml_input(prompt):
  result = ""
  line = input(prompt)
  if line.startswith("\\+"):
    line = line[1:]
  while True:
    if line.endswith("\\"):
      result += line[:-1] + "\n"
      line = input(">> ")
      continue
    else:
      result += line + "\n"
      break
  return result.rstrip()


def do_inference(instruction):
  try:
    for resp in cached_instruct_chain.stream(
      args={"instruction": instruction}
    ):
      if isinstance(resp, RespInfer):
        print(resp.tokstr, end="", flush=True)
    print("\n")
  except KeyboardInterrupt:
    print("(cut)")


while True:
  instruction = ml_input(">> Instruction: ")
  if instruction == "+load":
    with open(CACHE_STORE_FILE, "rb") as f:
      cached_instruct_chain.load_cache_store(pickle.load(f))
  elif instruction == "+save":
    with open(CACHE_STORE_FILE, "wb") as f:
      pickle.dump(cached_instruct_chain.save_cache_store(), f)
  elif instruction.startswith("+prompt "):
    with open(instruction[len("+prompt "):], "r", encoding="utf-8") as f:
      do_inference(f.read())
  else:
    do_inference(instruction)

import os
import pickle
import readline
import _import_helper
import _instruct_format
from langdash import Langdash
from langdash.infer import InferArgs
from langdash.response import RespInfer, RespReturns

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

prompt_format = _instruct_format.prompts[args["prompt_format"]]

accurate_infer_args = InferArgs(top_p=0.3)
creative_infer_args = InferArgs(
  min_new_tokens=10,
  typical_mass=0.8,
  rep_penalty=1.1,
)

instruct_chain = ld.chain(
  args={"instruction": str},
  returns={
    "title": str,
    "genre": str,
    "no_episodes": int,
    "prod_studio": str,
    "summary": str,
  },
  nodes=[
    ld.format_args(
      prompt_format.format(
        instruction="""\
Write the details and a brief summary for a completely new television show about the topic below:

{instruction}"""
      )
    ),
    "Title: ",
    ld.returns("title", "\n", infer_args=accurate_infer_args),
    "Genre: ",
    ld.returns("genre", "\n", infer_args=accurate_infer_args),
    "Production studio: ",
    ld.returns("prod_studio", "\n", infer_args=accurate_infer_args),
    "Number of episodes: ",
    ld.returns("no_episodes", "\n", infer_args=accurate_infer_args),
    "Plot: ",
    ld.returns("summary", ["", "\n"], infer_args=creative_infer_args),
  ],
)

instruct_chain = instruct_chain.cached("model")

if "CACHE_STORE_FILE" in os.environ:
  CACHE_STORE_FILE = os.path.abspath(os.environ["CACHE_STORE_FILE"])
else:
  import tempfile
  CACHE_STORE_FILE = os.path.join(tempfile.gettempdir(), "tv-show-cache.pkl")

print(
  f"""Special commands:
+load: load previous instruction cache
+save: save instruction cache to {CACHE_STORE_FILE}
"""
)


def ml_input(prompt):
  result = ""
  line = input(prompt)
  if line.startswith("\\+"):
    line = line[1:]
  while True:
    if line.endswith("\\"):
      result += line[:-1] + "\n"
      line = input(">> ")
      continue
    else:
      result += line + "\n"
      break
  return result.rstrip()


if "use_call" in os.environ:
  use_call = True
else:
  use_call = False

while True:
  instruction = ml_input(">> Instruction: ")
  if instruction == "+load":
    with open(CACHE_STORE_FILE, "rb") as f:
      instruct_chain.load_cache_store(pickle.load(f))
  elif instruction == "+save":
    with open(CACHE_STORE_FILE, "wb") as f:
      pickle.dump(instruct_chain.save_cache_store(), f)
  else:
    if use_call:
      print(instruct_chain.call(args={"instruction": instruction}))
    else:
      try:
        for resp in instruct_chain.stream(args={"instruction": instruction}):
          if isinstance(resp, RespReturns):
            print("\n")
            print(resp.key, end=": ", flush=True)
          elif isinstance(resp, RespInfer):
            print(resp.tokstr, end="", flush=True)
        print("\n")
      except KeyboardInterrupt:
        print("(cut)")

import _import_helper
from langdash import Langdash

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

color_choices = ld.chain(
  returns={"color": str},
  nodes=[
    "My favorite color is ",
    ld.choice("color", ["red", "green", "blue"])
  ]
)

session = ld.session_for_model("model")

with session.scratch_state():
  session.inject("I like red. Red is my favorite color. ")
  print(color_choices.call(session))

session.inject("I like blue. Blue is my favorite color. ")
print(color_choices.call(session))

import _import_helper
from langdash import Langdash
from langdash.search.embedding_search import EmbeddingSearch

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

db = EmbeddingSearch(ld.session_for_model("model"))

db.add(
  [
    "Oranges are delicious", "Apples are delicious",
    "Tokyo is the largest city in Japan", "The sky is blue",
    "An apple falls down from Tokyo Tower"
  ]
)

while True:
  query = input(">> Query: ")
  for i, doc, similarity in db.search(query):
    print(f"#{i} {doc}: {similarity}")

import os
import _import_helper
from langdash import Langdash
from langdash.infer import InferArgs
import langdash.chains.typing as chain_types

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

# Source: Measuring and Narrowing the Compositionality Gap in Language Models (arXiv:2210.03350)

init_search_prompt = ld.chain(
  args={
    "input": str
  },
  returns={
    "needs_followup": chain_types.Mapping({
      "Yes": True,
      "No": False,
    })
  },
  nodes=[
    ld.format_args(
      """\
Question: Who lived longer, Muhammad Ali or Alan Turing?
Are follow up questions needed here: Yes.
Follow up: How old was Muhammad Ali when he died?
Intermediate answer: Muhammad Ali was 74 years old when he died.
Follow up: How old was Alan Turing when he died?
Intermediate answer: Alan Turing was 41 years old when he died.
So the final answer is: Muhammad Ali

Question: When was the founder of craigslist born?
Are follow up questions needed here: Yes.
Follow up: Who was the founder of craigslist?
Intermediate answer: Craigslist was founded by Craig Newmark.
Follow up: When was Craig Newmark born?
Intermediate answer: Craig Newmark was born on December 6, 1952.
So the final answer is: December 6, 1952

Question: Who was the maternal grandfather of George Washington?
Are follow up questions needed here: Yes.
Follow up: Who was the mother of George Washington?
Intermediate answer: The mother of George Washington was Mary Ball Washington.
Follow up: Who was the father of Mary Ball Washington?
Intermediate answer: The father of Mary Ball Washington was Joseph Ball.
So the final answer is: Joseph Ball

Question: Are both the directors of Jaws and Casino Royale from the same country?
Are follow up questions needed here: Yes.
Follow up: Who is the director of Jaws?
Intermediate answer: The director of Jaws is Steven Spielberg.
Follow up: Where is Steven Spielberg from?
Intermediate answer: The United States.
Follow up: Who is the director of Casino Royale?
Intermediate answer: The director of Casino Royale is Martin Campbell.
Follow up: Where is Martin Campbell from?
Intermediate answer: New Zealand.
So the final answer is: No

Question: {input}
Are follow up questions needed here: """
    ),
    ld.choice(returns="needs_followup", choices=["Yes", "No"], argmax=True),
    ".\n",
  ]
).cached(
  "model", default_infer_args=InferArgs(top_p=0.8)
)

final_answer_field = "So the final answer is"
followup_field = ["Follow up", "Followup"]
intermediate_answer_field = "Intermediate answer"

field_inject_chain = ld.chain(
  args={"field": str},
  returns={"answer": str},
  nodes=[
    ld.format_args(f"{final_answer_field}: "),
    ld.returns("answer", "\n")
  ]
)

field_infer_chain = ld.chain(
  returns={
    "field": str,
    "value": str
  },
  nodes=[ld.returns("field", ":"),
         ld.returns("value", "\n", padleft=" ")]
)

while True:
  question = input("Question: ")
  result, session = init_search_prompt.call(
    args={"input": question}, return_session=True
  )
  needs_followup = result.returns["needs_followup"]
  print("Needs followup:", needs_followup)
  if needs_followup:
    for i in range(100):
      result = field_infer_chain.call(session)
      print(result)
      field, value = result.returns["field"], result.returns["value"]
      if field in followup_field:
        print("Followup:", value)
        print(
          "Intermediate answer:",
          field_inject_chain.call(
            session, args={
              "field": intermediate_answer_field
            }
          ).returns["answer"]
        )
      elif field == final_answer_field:
        print("Final answer:", value)
        break
      else:
        raise Exception(f"unexpected field: {field}")
  else:
    print(
      field_inject_chain.call(session, args={
        "field": final_answer_field
      }).returns["answer"]
    )

prompts = {
  "alpaca":
    """\
Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction:
{instruction}

### Response:
""",
  "wizardlm":
    """\
{instruction}

### Response:
""",
  "raw":
    """\
Question:

```
{instruction}
```

Answer:

```
"""
}

prompts_with_response = {k: v + "{response}" for k, v in prompts.items()}

prompts_end = {"raw": "```"}

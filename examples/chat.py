import _import_helper
from langdash import Langdash
from langdash.infer import InferArgs
from langdash.response import RespInfer
from langdash.llm_session import LLMGenerationSessionEvents

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

user_prompt = "User:"
bot_prompt = "Bot:"

init_prompt = f"""\
The following is a conversation between a highly knowledgeable and intelligent AI assistant called Bot, and a human user called User. In the following interactions, User and Bot converse in natural language, and Bot always answer User's questions. Bot is very smart, polite and humorous. Bot knows a lot, and always tells the truth. The conversation begins.

{user_prompt} who is president of usa?

{bot_prompt} It’s Joe Biden; he was sworn in earlier this year.

{user_prompt} french revolution what year

{bot_prompt} It started in 1789, but it lasted 10 years until 1799.

{user_prompt} guess i marry who ?

{bot_prompt} Only if you tell me more about yourself - what are your interests?

{user_prompt} wat is lhc

{bot_prompt} It’s a large and very expensive piece of science equipment. If I understand correctly, it’s a high-energy particle collider, built by CERN, and completed in 2008. They used it to confirm the existence of the Higgs boson in 2012.

{user_prompt}"""

input_then_response = ld.chain(
  args={"input": str},
  returns={"output": str},
  nodes=[
    ld.arg("input", padleft=" ", padright="\n\n"),
    f"{bot_prompt}:",
    ld.returns(
      "output",
      f"\n\n{user_prompt}",
      padleft=" ",
      infer_args=InferArgs(
        temperature=0.7,
        top_p=0.8,
        top_k=40,
      )
    ),
  ],
)


class ChatSessionEvents(LLMGenerationSessionEvents):

  def __init__(self):
    super().__init__()
    self.context_threshold = 0

  def on_node(self, node, args, tokens_used, session) -> bool:
    if self.context_threshold and session.tokens_used > self.context_threshold:
      print("Session reset")
      session.reset()
      return False


events = ChatSessionEvents()
session = ld.session_for_model("model", event_handlers=events)
if session.context_length > 0:
  events.context_threshold = session.context_length - 512

session.inject(init_prompt)
init_session = session.clone_state()

while True:
  instruction = input(">> Instruction: ")
  if instruction == "+reset":
    session.set_state(init_session)
    continue
  for resp in input_then_response.stream(
    ctx=session, args={"input": instruction}
  ):
    if isinstance(resp, RespInfer):
      print(resp.tokstr, end="", flush=True)
  print("\n")

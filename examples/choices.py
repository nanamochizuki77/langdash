import _import_helper
from langdash import Langdash

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)


def color_choices_static():
  color_choices = ld.chain(
    returns={
      "color": str
    },
    nodes=[
      "My favorite color is ",
      ld.choice("color", ["red", "green", "blue"])
    ]
  ).cached("model")

  print(color_choices.call())


def color_choices_dynamic():
  color_choices = ld.chain(
    args={
      "colors": list,
    },
    returns={
      "color": str
    },
    nodes=["My favorite color is ",
           ld.choice("color", "colors")]
  ).cached("model")
  print(color_choices.call(args={"colors": ["red", "green", "blue"]}))


color_choices_static()
color_choices_dynamic()

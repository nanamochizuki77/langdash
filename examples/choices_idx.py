import _import_helper
from langdash import Langdash

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

color_choices = ld.chain(
  returns={"color": int},
  nodes=[
    "I like blue. My favorite color is ",
    ld.choice("color", ["red", "green", "blue"], argmax=True, return_idx=True)
  ]
)

print(color_choices.call("model"))

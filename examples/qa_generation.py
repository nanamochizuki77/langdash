import _import_helper
from langdash import Langdash
from langdash.infer import InferArgs

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

document = "The Franco-Prussian War or Franco-German War, often referred to in France as the War of 1870, was a conflict between the Second French Empire and the North German Confederation led by the Kingdom of Prussia. Lasting from 19 July 1870 to 28 January 1871, the conflict was caused primarily by France's determination to reassert its dominant position in continental Europe, which appeared in question following the decisive Prussian victory over Austria in 1866. According to some historians, Prussian chancellor Otto von Bismarck deliberately provoked the French into declaring war on Prussia in order to induce four independent southern German states—Baden, Württemberg, Bavaria and Hesse-Darmstadt—to join the North German Confederation; other historians contend that Bismarck exploited the circumstances as they unfolded. All agree that Bismarck recognized the potential for new German alliances, given the situation as a whole."

questions = []
answers = []

infer_args = InferArgs(
  top_k=40,
  top_p=0.95,
)

q_item_chain = ld.chain(
  args={
    "index": int,
  },
  returns={
    "question": str,
  },
  nodes=[
    ld.format_args("{index}. "),
    ld.returns("question", "\n", infer_args=infer_args),
  ]
)

answer_chain = ld.chain(
  args={
    "question": str,
  },
  returns={
    "answer": str,
  },
  nodes=[
    ld.format_args(
      """\
Based on the document above, answer the following question:

{question}

Your answer should be in one line.

### Response:
"""
    ),
    "Answer: ",
    ld.returns("answer", "\n", infer_args=infer_args),
  ]
)

session = ld.session_for_model("model")

session.inject(f"""\
Document:

{document}

""")

with session.scratch_state():
  # Generate questions
  session.inject(
    """\
Create five (5) questions from the document above.
The questions should be insightful and be answerable based on the document alone.
Try to highlight different aspects of the document with each question.
Do not use vague pronouns such as 'it'.

### Response:
"""
  )

  for i in range(5):
    result = q_item_chain.call(ctx=session, args={"index": (i + 1)})
    print(result)
    questions.append(result.returns['question'])

# Generate answers
for question in questions:
  with session.scratch_state():
    result = answer_chain.call(ctx=session, args={"question": question})
    print(result)
    answers.append(result.returns['answer'])

# Final output
print("-" * 10)
print(questions)
print(answers)

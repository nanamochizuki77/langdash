import _import_helper
from langdash import Langdash
from langdash.infer import InferArgs
from langdash.logit_preprocessors import LogitPreprocessor
import re
from math import inf

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

session = ld.session_for_model("model")


class DigitFilter(LogitPreprocessor):

  def __init__(self, session):
    self.number_tokens = set()
    for tokstr, tokid in session.get_vocab().items():
      if re.match(r"^ *[0-9]+$", tokstr) is not None:
        self.number_tokens.add(tokid)

  def __call__(self, input_ids, logits):
    for token in range(len(logits)):
      if token not in self.number_tokens:
        logits[token] = -inf


chain = ld.chain(
  returns={"numbers": str},
  nodes=[
    "Here's a list of numbers from 1 to 10: ",
    ld.returns(
      "numbers",
      "",
      logit_preprocessors=[DigitFilter],
      infer_args=InferArgs(
        min_new_tokens=10,
        max_new_tokens=10,
      )
    )
  ]
)

for response in chain.stream(session):
  print(response)

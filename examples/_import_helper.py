import sys
import os
import pathlib
import re
import ast

KV_RE = re.compile(r"^([a-zA-Z_]\w*)=(.*)$")


def get_model_args():
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("type", type=str, help="model name")
  parser.add_argument(
    "arg", action="append", nargs="+", help="list of arguments to the model"
  )
  parser.add_argument(
    "-a",
    "--arg",
    action="append",
    nargs="*",
    help="list of arguments to the model"
  )
  parser.add_argument(
    "-ae",
    "--arg-eval",
    action="append",
    nargs="*",
    help="list of arguments to the model (evaluated with ast.literal_eval)"
  )
  parser.add_argument("--prompt-format", type=str, help="prompt format")

  raw_args = parser.parse_args()
  args = []
  kwargs = {}

  def add_to_arg(argdict, preprocess=lambda x: x):
    if argdict is None:
      return
    for arg in argdict:
      if len(arg) == 1:
        args.append(preprocess(arg[0]))
      elif len(arg) == 2:
        key, value = arg
        kwargs[key] = preprocess(value)

  add_to_arg(raw_args.arg)
  add_to_arg(raw_args.arg_eval, ast.literal_eval)

  return {
    "type": raw_args.type,
    "args": args,
    "kwargs": kwargs,
    "prompt_format": raw_args.prompt_format,
  }


sys.path.insert(0, str(pathlib.Path(os.path.abspath(__file__)).parent.parent))

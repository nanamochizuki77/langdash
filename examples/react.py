import os
import _import_helper
from typing import Set, Optional
from langdash import Langdash
from langdash.infer import InferArgs
from langdash.search.embedding_search import EmbeddingSearch

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)
ld.register_model(
  name="embedding_model",
  model=Langdash.model_from_type("sentence_transformers", "all-MiniLM-L6-v2")
)

# Prompting options
zero_shot = os.environ.get("zero_shot", "1") == "1"
inject_reminders = os.environ.get("inject_reminders", "") == "1"

default_infer_args = InferArgs(
  #  typical_mass=0.8,
  top_p=0.9,
  temperature=0.85,
)

if zero_shot:
  initial_react_chain = ld.chain(
    args={
      "question": str
    },
    nodes=[
      ld.format_args(
        """\
Your task is to answer a question step-by-step.

For each turn, list a Thought on what you will do next, the Tool, then the Tool Input, separated by a newline. You will be given an Observation.

You are given access to the following tools:

- <<Search>>: Search for a description. Be concise in your search. You should only search a single word or phrase.
- <<Finish>>: Return the answer to the question.

Remember, you only know common sense facts.
Please perform searches for every word you don't understand.
Continue to <<Search>> if you still don't know the answer to the question.

Question: {question}

### Response:
"""
      )
    ]
  ).cached(
    "model", default_infer_args=default_infer_args
  )

  # Original prompt: ReAct: Synergizing Reasoning and Acting in Language Models (arXiv:2210.03629)

  initial_thought_chain = ld.chain(
    args={
      "question": str,
    },
    returns={
      "thought": str
    },
    nodes=[
      ld.format_args(
        """\
Question: What is the elevation range for the area that the eastern sector of the Colorado orogeny extends into?
First thought: I need to search Colorado orogeny, find the area that the eastern sector of the Colorado orogeny extends into, then find the elevation range of the area.

Question: Musician and satirist Allie Goertz wrote a song about the "The Simpsons" character Milhouse, who Matt Groening named after who?
First thought: The question simplifies to "The Simpsons" character Milhouse is named after who. I only need to search Milhouse and find who it is named after.

Question: Which documentary is about Finnish rock groups, Adam Clayton Powell or The Saimaa Gesture?
First thought: I need to search Adam Clayton Powell and The Saimaa Gesture, and find which documentary is about Finnish rock groups.

Question: What profession does Nicholas Ray and Elia Kazan have in common?
First thought: I need to search Nicholas Ray and Elia Kazan, find their professions, then find the profession they have in common.

Question: Which magazine was started first Arthur’s Magazine or First for Women?
First thought: I need to search Arthur’s Magazine and First for Women, and find which was started first.

Question: {question}
First thought: """
      ),
      ld.returns("thought", "\n")
    ]
  ).cached(
    "model", default_infer_args=default_infer_args
  )
else:
  initial_react_chain = ld.chain(
    args={
      "question": str
    },
    nodes=[
      ld.format_args(
        """\
Question: Which documentary is about Finnish rock groups, Adam Clayton Powell or The Saimaa Gesture?

Thought: I need to search Adam Clayton Powell and The Saimaa Gesture, and find which documentary is about Finnish rock groups.
Tool: <<Search>>
Tool Input: Adam Clayton Powell
Observation: Could not find [Adam Clayton Powell]. Similar: ['Adam Clayton Powell III', 'Seventh Avenue (Manhattan)', 'Adam Clayton Powell Jr. State Office Building', 'Isabel Washington Powell', 'Adam Powell', 'Adam Clayton Powell (film)', 'Giancarlo Esposito'].

Thought: To find the documentary, I can search Adam Clayton Powell (film).
Tool: <<Search>>
Tool Input: Adam Clayton Powell (film)
Observation: "Adam Clayton Powell is a 1989 American documentary film directed by Richard Kilberg. The film is about the rise and fall of influential African-American politician Adam Clayton Powell Jr. It was later aired as part of the PBS series The American Experience."

Thought: Adam Clayton Powell (film) is a documentary about an African-American politician, not Finnish rock groups. So the documentary about Finnish rock groups must instead be The Saimaa Gesture.
Tool: <<Finish>>
Final Answer: The Saimaa Gesture

---

You will be given a question.
Use <<Search>> to search a description. Use <<Finish>> to return the answer to the question.

Question: {question}
"""
      ),
    ]
  ).cached(
    "model", default_infer_args=default_infer_args
  )

if inject_reminders:
  reminder_text = "Available tools: Use <<Search>> to search a description. Use <<Finish>> to return the answer to the question.\n"

THOUGHT_FIELD = "Thought"
TOOL_FIELD = "Tool"
TOOL_INPUT_FIELD = "Tool Input"
OBS_FIELD = "Observation"
FINAL_ANSWER_FIELD = "Final Answer"

TOOLS = ["Search"]
TOOLS_WITH_FINISH = TOOLS + ["Finish"]
MIN_TURNS = 2

turn_inject_input_chain = ld.chain(
  args={
    "field": str,
    "input": str,
  },
  nodes=[ld.format_args("{field}: "),
         ld.arg("input"), "\n"]
)

turn_get_output_chain = ld.chain(
  args={"field": str},
  returns={
    "output": str,
  },
  nodes=[
    ld.format_args("{field}: "),
    ld.returns("output", "\n"),
  ]
)

turn_tool_chain = ld.chain(
  returns={
    "tool": str,
  },
  nodes=[
    f"{TOOL_FIELD}: <<",
    ld.choice(returns="tool", choices=TOOLS),
    ">>\n",
  ]
)

turn_tool_with_finish_chain = ld.chain(
  returns={
    "tool": str,
  },
  nodes=[
    f"{TOOL_FIELD}: <<",
    ld.choice(returns="tool", choices=TOOLS_WITH_FINISH),
    ">>\n",
  ]
)

db = EmbeddingSearch(ld.session_for_model("embedding_model"))

db.add(
  [
    "Sprigatito is a Grass-type Pokémon introduced in Generation IX. Sprigatito is one of three starter Pokémon of Paldea available at the beginning of Pokémon Scarlet and Violet.",
    "Fuecoco is a Fire-type Pokémon introduced in Generation IX. Fuecoco is one of three starter Pokémon of Paldea available at the beginning of Pokémon Scarlet and Violet.",
    "Quaxly is a Water-type Pokémon introduced in Generation IX. Quaxly is one of three starter Pokémon of Paldea available at the beginning of Pokémon Scarlet and Violet.",
    "Bulbasaur is a dual-type Grass/Poison Pokémon introduced in Generation I. Bulbasaur is one of the three starter Pokémon of Kanto available at the beginning of Pokémon Red, Green, Blue, FireRed, and LeafGreen."
  ]
)


def search(argument: str, already_searched: Set[int]):
  if len(db) == len(already_searched):
    return -1, "All documents searched."
  for i, document, prob in db.search(argument, max_documents=-1):
    if i in already_searched:
      continue
    return i, f'"{document}"'
  return -1, "Information not available."


def do_turn(
  turn_number: int,
  session,
  already_searched: Set[int],
  initial_thought: Optional[str] = None
):
  if inject_reminders and turn_number > 0:
    session.inject(reminder_text)
  if initial_thought is not None:
    turn_inject_input_chain.call(
      session, args={
        "field": THOUGHT_FIELD,
        "input": initial_thought
      }
    )
    print("Thought:", initial_thought)
  else:
    thought = turn_get_output_chain.call(
      session, args={
        "field": THOUGHT_FIELD
      }
    ).returns["output"]
    print("Generated:", thought)
  if turn_number < MIN_TURNS:
    result = turn_tool_chain.call(session)
  else:
    result = turn_tool_with_finish_chain.call(session)
  tool = result.returns["tool"]
  print("Tool:", tool)
  if tool == "Search":
    tool_input = turn_get_output_chain.call(
      session, args={
        "field": TOOL_INPUT_FIELD
      }
    ).returns["output"]
    print("Tool Input:", tool_input)
    i, observation = search(tool_input, already_searched)
    if i != -1:
      already_searched.add(i)
    print("Observation:", observation)
    turn_inject_input_chain.call(
      session, args={
        "field": OBS_FIELD,
        "input": observation
      }
    )
    session.inject("\n")
    return True
  elif tool == "Finish":
    final_answer = turn_get_output_chain.call(
      session, args={
        "field": FINAL_ANSWER_FIELD
      }
    ).returns["output"]
    print("==>", final_answer)
    return False
  else:
    raise Exception(f"Unknown tool {tool}")


def main():
  while True:
    question = input(">>> Task: ")
    if zero_shot:
      _, session = initial_react_chain.call(
        args={"question": question}, return_session=True
      )
      print("(loaded initial session)")
      initial_thought = initial_thought_chain.call(
        args={
          "question": question
        }
      ).returns["thought"].strip()
    else:
      _, session = initial_react_chain.call(
        args={"question": question}, return_session=True
      )
      initial_thought = None
    try:
      already_searched = set()
      for i in range(100):
        if not do_turn(
          i, session, already_searched, initial_thought=initial_thought
        ):
          break
        if initial_thought:
          initial_thought = None
    except KeyboardInterrupt:
      continue


if __name__ == "__main__":
  main()

# Sample question: What does Fuecoco and Quaxly have in common?

import _import_helper
import _instruct_format
from langdash import Langdash
from langdash.classify.token_qa import TokenQA

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)

qa = TokenQA(
  prompt_chain=ld.chain(
    args={
      "query": str,
      "text": str
    },
    nodes=[
      ld.format_args(
        """\
Classify the following text to positive or negative.
Text: {query}
Sentiment: """
      )
    ]
  ).cached("model"),
  classes={
    "positive": ["Positive", "positive"],
    "negative": ["Negative", "negative"],
  }
)

while True:
  query = input(">> ")
  print(qa.query(query))

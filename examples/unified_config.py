import _import_helper
import os
from langdash import Langdash
from langdash.infer import InferArgs
from langdash.llm_config import LLMConfig

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    os.environ["model_type"],
    config=LLMConfig(
      model=os.environ["model_name"],
      batch_size=32,
      threads=1,
      context_length=-1,
      gpu_layers=0
    )
  ),
)

chain = ld.chain(
  returns={"result": str},
  nodes=[
    "The result of 1 + 1 = ",
    ld.returns("result", "", infer_args=InferArgs(max_new_tokens=1))
  ]
)

print(chain.call("model"))

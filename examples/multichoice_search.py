import _import_helper
import _instruct_format
from langdash import Langdash
from langdash.search.multichoice_search import MultichoiceSearch

args = _import_helper.get_model_args()

ld = Langdash()
ld.register_model(
  name="model",
  model=Langdash.model_from_type(
    args["type"], *args["args"], **args["kwargs"]
  ),
)


def document_prompt(search, key, document):
  return f"{key+1}", f"#{key+1}. {document}\n"


db = MultichoiceSearch(
  prompt_chain=ld.chain(
    args={
      "prompts": str,
      "query": str
    },
    nodes=[
      ld.format_args(
        _instruct_format.prompts_with_response[args["prompt_format"]].format(
          instruction="""\
The following is a list of facts, which can be used to answer a query:

{prompts}
Which fact can be used to best answer the following query?""",
          response="The query can be answered by fact #"
        )
      )
    ]
  ).cached("model", token_healing=False),
  document_prompt=document_prompt,
)

db.add(
  [
    "Oranges are delicious", "Apples are delicious",
    "Tokyo is the largest city in Japan", "The sky is blue",
    "An apple falls down from Tokyo Tower"
  ]
)

while True:
  query = input(">> ")
  for result in db.search(query, -1):
    print(result)

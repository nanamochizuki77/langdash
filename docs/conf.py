# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import sys
import os
import pathlib

repo_parent = pathlib.Path(os.path.abspath(__file__)).parent.parent
sys.path.insert(0, str(repo_parent))

import langdash

version = langdash.__version__
project = "langdash"
copyright = "2023, Nana Mochizuki"
author = "Nana Mochizuki"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
  "sphinx.ext.autodoc", "sphinx.ext.viewcode", "sphinx.ext.napoleon",
  "myst_parser", "autodocsumm"
]

autodoc_default_options = {
  "autosummary": True,
}

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"
html_static_path = ["_static"]

autodoc_mock_imports = [
  "torch",
  "faiss",
  "tokenizers",
  "llama_cpp",
  "transformers",
  "ctransformers",
  "sentence_transformers",
  "rwkv_cpp_model",
  "rwkv_cpp_shared_library",
  "rwkv_tokenizer",
  "tqdm",
]
autoclass_content = 'both'

# Search with embedding vectors

## Register the embedding model

Just like in the generation example, we initialize a Langdash instance and register an embedding model called "embedding_model". Then we create an `EmbeddingSearch` object to search for embeddings using this model. We will use the `all-MiniLM-L6-v2` served through the `sentence_transformers` backend.

```python
ld = Langdash()
ld.register_model(
  name="embedding_model",
  model=Langdash.model_from_type(type="sentence_transformers", model="all-MiniLM-L6-v2"),
)
```

## Search through the embeddings

Next, we create an embedding database, which stores a list of documents with their corresponding embeddings. The `db.add()` method is used to add multiple documents to the database.

```python
db = EmbeddingSearch(ld.session_for_model("embedding_model"))

db.add(
  [
    "Oranges are delicious", "Apples are delicious",
    "Tokyo is the largest city in Japan", "The sky is blue",
    "An apple falls down from Tokyo Tower"
  ]
)
```

Finally, we use `search()`, which takes a query and a parameter `max_documents` which specifies how many candidate documents to return for each search (which defaults to 1). Here the example "where did the apple go" is searched. The index, the content and the similarity scores for each document are then printed.

```python
query = "where did the apple go"
for i, doc, similarity in db.search(query):
  print(f"#{i} {doc}: {similarity}")
# example output: #2 An apple falls down from Tokyo Tower: 0.6126164197921753
```

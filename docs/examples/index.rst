Guides
========

.. toctree::
   :maxdepth: 1

   quick-start
   text-generation
   embedding-search
   unified-config
   generating-tv-shows
   document-searching
   scratch-state
   logit-preprocessor

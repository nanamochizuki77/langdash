.. include:: ./intro.md
   :parser: myst_parser.sphinx_

:Version: |version|

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   examples/index
   modules
   Repository <https://git.mysymphony.jp.net/nana/langdash/>
   Gitlab <https://gitlab.com/nanamochizuki77/langdash>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
